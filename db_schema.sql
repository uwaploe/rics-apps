CREATE TABLE IF NOT EXISTS gps (
       node INTEGER,    -- Node ID
       t INTEGER,       -- Seconds since 1/1/1970 UTC
       lat NUMERIC,     -- Latitude in degrees
       lon NUMERIC,     -- Longitude in degrees
       mode INTEGER     -- Fix mode: 1, 2, or 3
);
CREATE TABLE IF NOT EXISTS eng (
       node INTEGER,    -- Node ID
       t INTEGER,       -- Seconds since 1/1/1970 UTC
       vbatt NUMERIC,   -- Battery voltage in volts
       ibatt NUMERIC,   -- Battery current in amps
       temp NUMERIC     -- Internal temperature in degC
);
CREATE TABLE IF NOT EXISTS diag (
       node INTEGER,    -- Node ID
       t INTEGER,       -- Seconds since 1/1/1970 UTC
       contents BLOB    -- Diagnostics data record
);
CREATE TABLE IF NOT EXISTS rads (
       t INTEGER,       -- Seconds since 1/1/1970 UTC
       contents BLOB    -- Data record sent to RADS
);
CREATE TABLE IF NOT EXISTS grid (
       t INTEGER,       -- Seconds since 1/1/1970 UTC
       lat NUMERIC,     -- Origin latitude in degrees
       lon NUMERIC,     -- Origin longitude in degrees
       az NUMERIC       -- Azimuth in degrees True
);
CREATE TABLE IF NOT EXISTS xypos (
       node INTEGER,    -- Node ID
       t INTEGER,       -- Seconds since 1/1/1970 UTC
       x NUMERIC,       -- Grid X coordinate in us-yards
       y NUMERIC        -- Grid y coordinate in us-yards
);
CREATE TABLE IF NOT EXISTS track (
       t INTEGER,       -- Seconds since 1/1/1970 UTC
       target TEXT,     -- Target name
       x NUMERIC,       -- Grid X coordinate in us-yards
       y NUMERIC,       -- Grid y coordinate in us-yards
       lat NUMERIC,     -- Latitude in degrees
       lon NUMERIC      -- Longitude in degrees
);
