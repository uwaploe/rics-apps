#!/usr/bin/env python
#
"""
RICS system controller process.
"""
import sys
import signal
import zmq
import argparse
import logging
import time
import json
from collections import namedtuple, OrderedDict
from netifaces import AF_INET, gateways, ifaddresses, interfaces
from netaddr import IPNetwork, IPAddress


CTL_PORT = 10133
BEACON_PORT = 10131


def signal_handler(signal, frame):
    sys.exit(0)


def find_ip(dest):
    """
    Look-up the IP address of the network interface that
    should be used to route to the address of the peer
    at "dest".
    """
    dest = IPAddress(dest)
    for i in interfaces():
        # Get all AF_INET entries for an interface
        addrs = ifaddresses(i).get(AF_INET)
        # Determine the network associated with each interface
        # and check if the destination is part of that same
        # network. If it is, we're done...
        if addrs:
            for a in addrs:
                if 'netmask' in a:
                    net = IPNetwork('{0}/{1}'.format(
                        a['addr'], a['netmask']))
                    if dest in net:
                        return a['addr']
    # The destination is not on a directly connected network, find
    # the interface associated with our default route and use that
    # interface's IP address.
    gw, iface = gateways()['default'][AF_INET]
    gw = IPAddress(gw)
    for a in ifaddresses(iface)[AF_INET]:
        if 'netmask' in a:
            net = IPNetwork('{0}/{1}'.format(
                a['addr'], a['netmask']))
            if gw in net:
                return a['addr']
    return None


RTRNode = namedtuple('RTRNode', 'sock addr port active')


class RTRSet(object):
    """
    Manage the network control interface for a set of
    RTR nodes.
    """
    def __init__(self, context=None, autostart=False):
        self.ctx = context or zmq.Context.instance()
        self.autostart = autostart
        self.nodes = dict()
        self.cfg = dict()

    def __contains__(self, key):
        return key in self.nodes

    def __iter__(self):
        return iter(self.nodes.keys())

    def get_config(self, rtr_id):
        """
        Return the configuration dictionary for an RTR node.
        """
        return self.cfg.get(rtr_id, {})

    def update_config(self, rtr_id, settings):
        """
        Update the configuration settings for an RTR node.

        :param rtr_id: RTR node ID
        :type rtr_id: int
        :param settings: space-separated *name=value* settings
        :type settings: string
        """
        for setting in settings.split():
            k, v = setting.split('=')
            self.cfg[rtr_id][k] = v

    def add_node(self, rtr_id, ipaddr, port):
        """
        Add a new RTR node to the set. The socket connection
        is established and, if *autostart* is `True`, a `HELLO`
        command is sent to start the data-stream.

        :param rtr_id: RTR node ID
        :type rtr_id: int
        :param ipaddr: RTR node IP address
        :type ipaddr: string
        :param port: RTR node command port
        :type port: int
        """
        url = 'tcp://{0}:{1:d}'.format(ipaddr, port)
        s = self.ctx.socket(zmq.DEALER)
        s.connect(url)
        logging.info('Adding RTR_ID:%d @%s', rtr_id, url)
        self.nodes[rtr_id] = RTRNode(sock=s, addr=ipaddr, port=port,
                                     active=False)
        if self.autostart:
            logging.info('Autostarting data-stream on RTR_ID:%d', rtr_id)
            s.send_multipart([b'HELLO', find_ip(ipaddr)])
            resp = s.recv_multipart()
            logging.debug('RTR response: %r', resp)
            if resp[0] == b'HELLO':
                self.nodes[rtr_id] = self.nodes[rtr_id]._replace(active=True)
                self.cfg[rtr_id] = OrderedDict()
                self.update_config(rtr_id, resp[1])
            else:
                s.send_multipart([b'BYE', b''])
                s.recv_multipart()
                self.del_node(rtr_id)

    def del_node(self, rtr_id):
        """
        Remove an RTR node from the set. The caller is responsible for
        sending the `BYE` command to stop the data-stream.

        :param rtr_id: RTR node ID
        :type rtr_id: int
        """
        if rtr_id in self.nodes:
            logging.info('Removing RTR_ID:%d', rtr_id)
            self.nodes[rtr_id].sock.close(linger=0)
            del self.nodes[rtr_id]

    def send(self, rtr_id, cmd, args):
        """
        Send a command to an RTR node.

        :param rtr_id: RTR node ID
        :type rtr_id: int
        :param cmd: command string
        :param args: argument string
        :returns: response message as a sequence of strings
        :raises: KeyError if *rtr_id* is invalid.
        """
        if rtr_id in self.nodes:
            s = self.nodes[rtr_id].sock
            if cmd == b'HELLO':
                args = find_ip(self.nodes[rtr_id].addr)
            logging.debug('SEND: [%r, %r]', cmd, args)
            s.send_multipart([cmd, args])
            resp = s.recv_multipart()
            logging.debug('RECV: %r', resp)
            if resp[0] == b'HELLO':
                self.nodes[rtr_id] = self.nodes[rtr_id]._replace(active=True)
                self.cfg[rtr_id] = OrderedDict()
                self.update_config(rtr_id, resp[1])
            if resp[0] in (b'SET', b'GET'):
                self.update_config(rtr_id, resp[1])
            if resp[0] == b'BYE':
                self.del_node(rtr_id)
            return resp
        else:
            raise KeyError('Unknown RTR ID')

    def ids(self):
        """
        Return a list of RTR node IDs.
        """
        return self.nodes.keys()


# Command handlers.
def cmd_FORGET(args, rtrs):
    for rtr_id in [int(r) for r in args.split()]:
        rtrs.del_node(rtr_id)
    return [b'NODES',
            b' '.join([str(r).encode('us-ascii') for r in rtrs.ids()])]


def cmd_NODES(args, rtrs):
    return [b'NODES',
            b' '.join([str(r).encode('us-ascii') for r in rtrs.ids()])]


def cmd_CFG(args, rtrs):
    rtr_id = int(args)
    d = rtrs.get_config(rtr_id)
    body = b' '.join(['{}={}'.format(k, v) for k, v in d.items()])
    return [b'CFG', body.encode('us-ascii')]


def cmd_AUTOSTART(args, rtrs):
    if args == b'ON':
        rtrs.autostart = True
    elif args == b'OFF':
        rtrs.autostart = False
    return [b'AUTOSTART',
            b'ON' if rtrs.autostart else b'OFF']


def main():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='show debugging output')
    parser.add_argument('--beacon-url',
                        default='tcp://localhost:{0:d}'.format(BEACON_PORT),
                        help='url for beacon publisher (%(default)s)')
    parser.add_argument('--cmd-port',
                        type=int,
                        default=CTL_PORT,
                        help='TCP port for command interface (%(default)s)')
    parser.add_argument('--no-autostart',
                        action='store_false',
                        dest='autostart',
                        help='disable RTR auto-start')
    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(level=level,
                        format='%(levelname)-8s %(message)s')

    ctx = zmq.Context.instance()
    sub = ctx.socket(zmq.SUB)
    sub.connect(args.beacon_url)
    sub.setsockopt(zmq.SUBSCRIBE, b'')
    ctl = ctx.socket(zmq.ROUTER)
    ctl.bind('tcp://*:{0:d}'.format(args.cmd_port))

    poller = zmq.Poller()
    poller.register(sub, zmq.POLLIN)
    poller.register(ctl, zmq.POLLIN)

    rtrs = RTRSet(context=ctx, autostart=args.autostart)
    G = globals()
    t_start = time.time()

    signal.signal(signal.SIGTERM, signal_handler)

    try:
        while True:
            socks = dict(poller.poll())
            if socks.get(sub) == zmq.POLLIN:
                beacon = sub.recv_json()
                logging.debug('>SUB: %r', beacon)
                rtr_id = beacon['rtr']
                if rtr_id not in rtrs:
                    rtrs.add_node(rtr_id,
                                  beacon['ipaddr'],
                                  beacon['cmdport'])

            if socks.get(ctl) == zmq.POLLIN:
                msg = ctl.recv_multipart()
                logging.debug('>ROUTER: %r', msg)
                try:
                    ident, t, rtr, cmd, args = msg
                    # Ignore old messages
                    if json.loads(t) < t_start:
                        logging.info('Ignoring stale message')
                        continue
                    if rtr:
                        rtr_id = int(rtr)
                        resp = rtrs.send(rtr_id, cmd, args)
                        ctl.send_multipart([ident, rtr, resp[0], resp[1]],
                                           flags=zmq.NOBLOCK)
                    else:
                        func = G.get('cmd_'+cmd)
                        if func:
                            resp = func(args, rtrs)
                            # For the CFG response, pretend that it
                            # came from the RTR.
                            if cmd == 'CFG':
                                rtr = args
                            if resp:
                                ctl.send_multipart([ident, rtr,
                                                    resp[0],
                                                    resp[1]],
                                                   flags=zmq.NOBLOCK)
                except KeyError:
                    ctl.send_multipart([ident,
                                        rtr,
                                        b'ERROR',
                                        b'Invalid RTR ID: ' + rtr])
                except zmq.ZMQError:
                    logging.exception('Cannot reply to %r', ident)
                except Exception:
                    logging.exception('RTR send')
                    ctl.send_multipart([ident,
                                        rtr,
                                        b'ERROR',
                                        b'Cannot forward: ' + repr(msg)])

    except (KeyboardInterrupt, SystemExit):
        logging.info('Exiting ...')
    except Exception:
        logging.exception('Exiting ...')
    finally:
        for rtr_id in rtrs:
            rtrs.send(rtr_id, b'BYE', b'')
        sub.close(linger=0)
        ctl.close(linger=0)


if __name__ == '__main__':
    main()
