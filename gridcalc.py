#!/usr/bin/env python
"""
Calculate the Grid X-Y location of the RTR Nodes and send the
information to the Data Manager.
"""
import sys
import signal
import zmq
import logging
import json
import struct
import argparse as AP
from pyicex.geo import Grid, Point


PUB_PORT = 10132
DSTREAM_PORT = 10130


def signal_handler(signal, frame):
    sys.exit(0)


def create_xy_msg(t, rtr_id, x, y):
    hdr = struct.pack('>cBll', b'X', rtr_id, t, 0)
    body = json.dumps({'x': str(round(x, 2)), 'y': str(round(y, 2))})
    return [hdr, body.encode('us-ascii')]


def main():
    parser = AP.ArgumentParser(epilog=__doc__)
    parser.add_argument('--pub-url',
                        default='tcp://localhost:{0:d}'.format(PUB_PORT),
                        help='url for Publisher (%(default)s)')
    parser.add_argument('--data-url',
                        default='tcp://localhost:{0}'.format(DSTREAM_PORT),
                        help='url for data-stream (%(default)s)')
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='show debugging output')

    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(level=level,
                        format='%(levelname)-8s %(message)s')

    ctx = zmq.Context.instance()
    dstream = ctx.socket(zmq.PUSH)
    dstream.set_hwm(1)
    dstream.connect(args.data_url)

    # Connect to the publisher and subscribe to GPS and GRID messages
    sub = ctx.socket(zmq.SUB)
    sub.connect(args.pub_url)
    sub.setsockopt(zmq.SUBSCRIBE, b'GPS')
    sub.setsockopt(zmq.SUBSCRIBE, b'GRID')

    signal.signal(signal.SIGTERM, signal_handler)

    logging.info('Listening for messages')

    grid = None
    try:
        while True:
            mtype, body = sub.recv_multipart()
            logging.debug('> %r, %r', mtype, body)
            msg = json.loads(body)
            if mtype == b'GRID':
                grid = Grid(Point(lat=float(msg['lat']),
                                  lon=float(msg['lon'])),
                            float(msg['az']),
                            units='us-yd')
            elif mtype == b'GPS':
                if grid:
                    gp = grid.toxy(Point(lat=float(msg['lat']),
                                         lon=float(msg['lon'])))
                    try:
                        xy = create_xy_msg(msg['time'], msg['rtr'], gp.x, gp.y)
                        logging.debug('< %r', xy)
                        dstream.send_multipart(xy, flags=zmq.NOBLOCK)
                    except zmq.ZMQError:
                        logging.info('Data Manager offline')
                    except Exception:
                        logging.exception('Message format error')
    except (KeyboardInterrupt, SystemExit):
        logging.info('Exiting ...')
    finally:
        sub.close(linger=0)
        dstream.close(linger=0)


if __name__ == '__main__':
    main()
