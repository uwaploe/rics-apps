#!/usr/bin/env python
#
# Run the GICS Monitor with simulated serial port input.
#
import os
import pty
import serial
import argparse
import time
import subprocess


def main():
    """
    GICS simulator.
    """
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument('lat',
                        type=float,
                        metavar='DEG',
                        help='latitude in degrees')
    parser.add_argument('lon',
                        type=float,
                        metavar='DEG',
                        help='longitude in degrees')
    parser.add_argument('az',
                        type=float,
                        metavar='DEG',
                        help='azimuth in degrees True')
    parser.add_argument('-i', '--interval',
                        type=int,
                        default=30,
                        metavar='SECS',
                        help='data interval (%(default)d)')
    args = parser.parse_args()

    master, slave = pty.openpty()
    proc = subprocess.Popen(['./gicsmon.py', '-v', os.ttyname(slave)])

    try:
        while True:
            ts = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime())
            os.write(master,
                     '{} {:.6f} {:.6f} {:.1f}\r\n'.format(ts,
                                                          args.lat,
                                                          args.lon,
                                                          args.az))
            time.sleep(args.interval)
    except KeyboardInterrupt:
        proc.terminate()
        proc.wait()


if __name__ == '__main__':
    main()
