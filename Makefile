#
# Install the RICS applications and configuration files.
#
SHELL = /bin/bash
BINDIR = $(HOME)/bin
CFGDIR = $(HOME)/.config/rics

PROGS := beaconmon.py datamgr.py dirsmon.py ricspub.py \
         controller.py gicsmon.py gridcalc.py gpstrack.py
CFG := db_schema.sql

.PHONY: install install-cfg install-bin

all: install

install: install-cfg install-bin

install-cfg: $(CFG)
	install -d $(CFGDIR)
	install -m 644 -t $(CFGDIR) $^

install-bin: $(PROGS)
	install -d $(BINDIR)
	install -m 755 -t $(BINDIR) $^
