#!/usr/bin/env python
"""
Convert a real-time X-Y track from RADS to a lat-lon track
and send the information to the Data Manager. The X-Y values
are read from the RADS serial output.
"""
import sys
import signal
import zmq
import logging
import json
import struct
import argparse
import serial
import time
import math
from threading import Thread, Event
from calendar import timegm
from pyicex.geo import Grid, Point, GridPoint


PUB_PORT = 10132
DSTREAM_PORT = 10130
# Date-time format in the X-Y data records
TIMEFMT = '%d/%m/%Y %H:%M:%S %Z'
# ZeroMQ URL for X-Y conversion thread
CMDURL = 'inproc://cmd'


def signal_handler(signal, frame):
    sys.exit(0)


def parse_xy(line):
    """
    Parse an X-Y data record from RADS.
    """
    fields = line.strip().split()
    if len(fields) != 10:
        logging.debug('Bad record length: %r', line)
        return None, None
    try:
        t = timegm(time.strptime(' '.join([fields[0], fields[1], 'UTC']),
                                 TIMEFMT))
    except ValueError:
        logging.info('Invalid record: %r', line)
        return None, None
    x = fields[2::2]
    y = fields[3::2]
    return t, zip(x, y)


def create_track_packet(t, geo, xy):
    """
    Generator to create a set of (up to) four Track packets.
    """
    target_labels = ['f1q1', 'f1q2', 'f2q1', 'f2q2']
    hdr = struct.pack('>cBll', b'R', 0, t, 0)
    for tgt, p, gp in zip(target_labels, geo, xy):
        if gp[0] != 'NaN':
            body = json.dumps(dict(target=tgt, lon=p[0], lat=p[1],
                                   x=gp[0], y=gp[1]))
            yield [hdr, body]


def xyconvert(ev, puburl, cmdurl, grid0=None, context=None):
    """
    Thread to subscribe to GRID messages from the Data Manager and
    use this information to convert X-Y coordinates to LAT-LON.
    """
    ctx = context or zmq.Context.instance()
    sub = ctx.socket(zmq.SUB)
    sub.setsockopt(zmq.SUBSCRIBE, b'GRID')
    sub.connect(puburl)
    cmd = ctx.socket(zmq.PAIR)
    cmd.connect(cmdurl)

    poller = zmq.Poller()
    poller.register(sub, zmq.POLLIN)
    poller.register(cmd, zmq.POLLIN)

    logging.info('Listening for messages')

    grid = grid0
    while not ev.is_set():
        socks = dict(poller.poll())
        if socks.get(sub) == zmq.POLLIN:
            _, body = sub.recv_multipart()
            logging.debug('>SUB: %r', body)
            msg = json.loads(body)
            grid = Grid(Point(lat=float(msg['lat']),
                              lon=float(msg['lon'])),
                        float(msg['az']),
                        units='us-yd')
        if socks.get(cmd) == zmq.POLLIN:
            msg = cmd.recv_json()
            logging.debug('>PAIR: %r', msg)
            if grid:
                result = []
                # Convert each x,y pair to lon,lat (note order)
                for x, y in msg:
                    gp = GridPoint(x=float(x), y=float(y))
                    if not math.isnan(gp.x):
                        p = grid.tolatlon(gp)
                        result.append([str(round(p.lon, 6)),
                                       str(round(p.lat, 6))])
                    else:
                        result.append(['NaN', 'NaN'])
                logging.debug('<PAIR: %r', result)
                cmd.send_json(result)
            else:
                logging.info('No Grid data available')
                cmd.send_json([])
    logging.info('Exiting')
    cmd.close(linger=0)
    sub.close(linger=0)


def main():
    """
    Convert track from X-Y to LAT-LON in real-time.
    """
    parser = argparse.ArgumentParser(description=main.__doc__,
                                     epilog=__doc__)
    parser.add_argument('port', help='serial port device')
    parser.add_argument('-b', '--baud',
                        type=int,
                        default=9600,
                        help='serial port baud rate (%(default)d)')
    parser.add_argument('--pub-url',
                        default='tcp://localhost:{0:d}'.format(PUB_PORT),
                        help='url for Publisher (%(default)s)')
    parser.add_argument('--data-url',
                        default='tcp://localhost:{0}'.format(DSTREAM_PORT),
                        help='url for data-stream (%(default)s)')
    parser.add_argument('--grid',
                        metavar='LAT,LON,AZ',
                        help='initial Grid specification')
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='show debugging output')

    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(level=level,
                        format='%(levelname)-8s %(threadName)-10s %(message)s')

    try:
        p = serial.Serial(args.port, baudrate=args.baud)
        logging.info('Opening serial port %r', args.port)
    except (serial.SerialException, OSError):
        p = open(args.port, 'r')
        logging.info('Opening file for X-Y data %r', args.port)

    if args.grid:
        lat, lon, az = [float(x) for x in args.grid.split(',')]
        grid0 = Grid(Point(lat=lat, lon=lon), az, units='us-yd')
    else:
        grid0 = None

    ctx = zmq.Context.instance()
    cmd = ctx.socket(zmq.PAIR)
    cmd.bind(CMDURL)

    dstream = ctx.socket(zmq.PUSH)
    dstream.set_hwm(4)
    dstream.connect(args.data_url)

    ev = Event()
    ev.clear()
    task = Thread(target=xyconvert,
                  name='xyconvert',
                  args=(ev, args.pub_url, CMDURL),
                  kwargs={'grid0': grid0})
    task.daemon = True
    task.start()

    signal.signal(signal.SIGTERM, signal_handler)

    logging.info('Waiting for data records')
    try:
        while True:
            line = p.readline()
            logging.debug('> %r', line)
            if line and line[0] != 'X':
                t, xy = parse_xy(line)
                if t is not None:
                    cmd.send_json(xy)
                    geo = cmd.recv_json()
                    if geo:
                        for pkt in create_track_packet(t, geo, xy):
                            logging.debug('<PUSH: %r', pkt)
                            try:
                                dstream.send_multipart(pkt, zmq.NOBLOCK)
                            except zmq.Again:
                                logging.critical('Server down')
    except (KeyboardInterrupt, SystemExit):
        logging.info('Exiting ...')
    except Exception:
        logging.exception('Exiting ... ')
    finally:
        ev.set()
        task.join(3)
        dstream.close(linger=0)
        p.close()


if __name__ == '__main__':
    main()
