#!/usr/bin/env python
"""
Subscribe to messages from the Data Manager and publish
them to a Redis channel.
"""
import sys
import signal
import zmq
import redis
import json
import logging
import argparse as AP


PUB_PORT = 10132


def signal_handler(signal, frame):
    sys.exit(0)


def main():
    parser = AP.ArgumentParser(epilog=__doc__)
    parser.add_argument('channel',
                        help='Redis channel name')
    parser.add_argument('--host',
                        default='localhost',
                        help='Redis host (%(default)s)')
    parser.add_argument('--port',
                        type=int,
                        default=6379,
                        help='Redis TCP port (%(default)d)')
    parser.add_argument('--db',
                        type=int,
                        default=0,
                        help='Redis DB number (%(default)d)')
    parser.add_argument('--pub-url',
                        default='tcp://localhost:{0:d}'.format(PUB_PORT),
                        help='url for Publisher (%(default)s)')
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='show debugging output')

    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(level=level,
                        format='%(levelname)-8s %(message)s')

    rd = redis.StrictRedis(host=args.host,
                           port=args.port,
                           db=args.db)
    logging.info('Connected to Redis')

    # Connect to the publisher and subscribe to all messages.
    ctx = zmq.Context.instance()
    sub = ctx.socket(zmq.SUB)
    sub.connect(args.pub_url)
    sub.setsockopt(zmq.SUBSCRIBE, b'')

    signal.signal(signal.SIGTERM, signal_handler)
    logging.info('Listening for messages')

    try:
        while True:
            mtype, body = sub.recv_multipart()
            logging.debug('< %r, %r', mtype, body)
            msg = json.loads(body)
            rd.publish(args.channel, json.dumps([mtype, msg]))
    except (KeyboardInterrupt, SystemExit):
        logging.info('Exiting ...')
    finally:
        sub.close(linger=0)
        rd.close()


if __name__ == '__main__':
    main()
