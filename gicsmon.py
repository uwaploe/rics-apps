#!/usr/bin/env python
#
# Read grid orientation records from GICS over a serial link
# and convert to data-stream packets for the RICS server.
#
import sys
import signal
import serial
import zmq
import logging
import time
import argparse
import struct
import json
from calendar import timegm


RICS_PORT = 10130
TIMEFMT = '%Y-%m-%d %H:%M:%S %Z'
RECLEN = 5
packer = struct.Struct('>cBll')


def signal_handler(signal, frame):
    sys.exit(0)


def create_data_packet(rec):
    """
    """
    names = ('lat', 'lon', 'az')
    fields = rec.strip().split()
    if len(fields) != RECLEN:
        logging.info('Bad record length: %r', rec)
        return
    try:
        t = timegm(time.strptime(' '.join([fields[0], fields[1], 'UTC']),
                                 TIMEFMT))
    except ValueError:
        logging.info('Invalid record: %r', rec)
        return []
    # Packet header.
    hdr = packer.pack(b'G', 0, t, 0)
    # Body consists of the remainder of the fields stored in a JSON
    # encoded dictionary. We store the fields as strings to avoid
    # changing the precision of the floating-point values.
    body = dict(zip(names, fields[2:]))
    return [hdr, json.dumps(body)]


def main():
    parser = argparse.ArgumentParser(
        description='Read serial stream from GICS')
    parser.add_argument('port', help='serial port device')
    parser.add_argument('-b', '--baud',
                        type=int,
                        default=115200,
                        help='serial port baud rate')
    parser.add_argument('--host',
                        default='localhost',
                        help='RICS server host')
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        help='show debugging output')
    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(level=level,
                        format='%(levelname)-8s %(message)s')

    try:
        p = serial.Serial(args.port, baudrate=args.baud)
        logging.info('Opening serial port %r', args.port)
    except (serial.SerialException, OSError):
        p = open(args.port, 'r')
        logging.info('Opening file for GICS data %r', args.port)

    ctx = zmq.Context.instance()
    dstream = ctx.socket(zmq.PUSH)
    dstream.set_hwm(10)
    logging.info('Connecting to server')
    dstream.connect('tcp://{0}:{1}'.format(args.host,
                                           RICS_PORT))

    signal.signal(signal.SIGTERM, signal_handler)

    logging.info('Waiting for data records')
    try:
        while True:
            line = p.readline()
            if not line:
                logging.debug('No data')
                continue
            logging.debug('RECV: %r', line)
            try:
                pkt = create_data_packet(line)
                if pkt:
                    dstream.send_multipart(pkt, zmq.NOBLOCK)
            except zmq.Again:
                logging.critical('Server down')

    except (KeyboardInterrupt, SystemExit):
        logging.info('Exiting ...')
    except Exception:
        logging.exception('Exiting ... ')
    finally:
        dstream.close(linger=0)
        p.close()


if __name__ == '__main__':
    main()
