#!/usr/bin/env python
#
# Monitor the network for RTR UDP beacon packets and publish them
# on a ZeroMQ socket.
#
import sys
import signal
import zmq
import argparse
import socket
import logging
import struct


BEACON_PORT = 10130
DSTREAM_PORT = 10130
PUB_PORT = 10131


def signal_handler(signal, frame):
    sys.exit(0)


def create_gps_msg(data):
    """
    Create a GPS data-stream message from the contents of a
    Beacon packet.
    """
    hdr = struct.pack('>cBll', b'P', data['rtr'], data['time'], 0)
    body = b'{0:.6f} {1:.6f} {2:d}'.format(data['slat']/1000000.,
                                           data['slon']/1000000.,
                                           data['mode'])
    return [hdr, body]


def create_eng_msg(data):
    """
    Create an Engineering data-stream message from the contents of a
    Beacon packet.
    """
    t, rest = data['trailer'].split(',', 1)
    secs, usecs = divmod(long(float(t) * 1000000), 1000000)
    hdr = struct.pack('>cBll', b'E', data['rtr'], secs, usecs)
    return [hdr, rest]


def publish_beacon(pub_endpoint, push_endpoint='tcp://localhost:10130',
                   context=None):
    keys = ('rtr', 'time', 'slat', 'slon', 'mode', 'cmdport')
    ctx = context or zmq.Context.instance()
    pub = ctx.socket(zmq.PUB)
    pub.set_hwm(1)
    pub.bind(pub_endpoint)
    dstream = ctx.socket(zmq.PUSH)
    dstream.set_hwm(4)
    dstream.connect(push_endpoint)

    # Initialize UDP socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    s.bind(('', BEACON_PORT))
    poller = zmq.Poller()
    poller.register(s.fileno(), zmq.POLLIN)
    logging.info('Listening for RTR beacons')
    while True:
        socks = dict(poller.poll())
        if socks.get(s.fileno()) == zmq.POLLIN:
            buf, addrinfo = s.recvfrom(256)
            fields = struct.unpack('>B3xlllB3xH', buf[:22])
            d = dict(zip(keys, fields))
            d['ipaddr'] = addrinfo[0]
            n = ord(buf[22])
            d['trailer'], = struct.unpack('{0}p'.format(n+1), buf[22:])
            try:
                pub.send_json(d, flags=zmq.NOBLOCK)
                logging.debug('PUB: %r', d)
                dstream.send_multipart(create_gps_msg(d),
                                       flags=zmq.NOBLOCK)
                if n > 0:
                    logging.debug('Got engineering data')
                    dstream.send_multipart(create_eng_msg(d),
                                           flags=zmq.NOBLOCK)
            except zmq.ZMQError:
                logging.info('Data Manager offline')


def main():
    parser = argparse.ArgumentParser(description='Monitor RTR beacon')
    parser.add_argument('--data-url',
                        default='tcp://localhost:{0}'.format(DSTREAM_PORT),
                        help='url for data-stream (%(default)s)')
    parser.add_argument('--port',
                        type=int,
                        default=PUB_PORT,
                        help='TCP port for publisher (%(default)s)')
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='show debugging output')
    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(level=level,
                        format='%(levelname)-8s %(message)s')

    signal.signal(signal.SIGTERM, signal_handler)

    logging.info('Starting monitor')
    try:
        publish_beacon('tcp://*:{0:d}'.format(args.port),
                       push_endpoint=args.data_url)
    except (KeyboardInterrupt, SystemExit):
        pass
    except Exception:
        logging.exception('ERROR')
    logging.info('Exiting ...')


if __name__ == '__main__':
    main()
