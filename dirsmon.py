#!/usr/bin/env python
#
# Read timestamp records from DIRS over a serial link and
# create RTR-compatible timestamp data records for the RICS
# server.
#
import sys
import signal
import serial
import zmq
import logging
import time
import argparse
import struct
from calendar import timegm


RICS_PORT = 10130
TIMEFMT = '%d/%m/%Y %H:%M:%S %Z'

packer = struct.Struct('>cBll')


def signal_handler(signal, frame):
    sys.exit(0)


def create_data_packets(rec):
    """
    Generate a set of four time-stamp Data Stream packets
    from a DIRS data record.
    """
    fields = rec.strip().split()
    if len(fields) != 18:
        logging.info('Bad record length: %r', rec)
        return
    try:
        t = timegm(time.strptime(' '.join([fields[0], fields[1], 'UTC']),
                                 TIMEFMT))
    except ValueError:
        logging.info('Invalid record: %r', rec)
        return
    # Generate a separate data packet for each hydrophone.
    i, j = 2, 10
    for k in range(4):
        data = ' '.join([fields[i+k], fields[j+k]])
        hdr = packer.pack(b'T', 0x81+k, t, 0)
        logging.debug('SEND: %r', data)
        yield [hdr, data]


def main():
    parser = argparse.ArgumentParser(
        description='Read serial stream from DIRS')
    parser.add_argument('port', help='serial port device')
    parser.add_argument('-b', '--baud',
                        type=int,
                        default=115200,
                        help='serial port baud rate')
    parser.add_argument('--host',
                        default='localhost',
                        help='RICS server host')
    parser.add_argument('-v', '--verbose',
                        action='store_true',
                        help='show debugging output')
    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(level=level,
                        format='%(levelname)-8s %(message)s')

    try:
        p = serial.Serial(args.port, baudrate=args.baud)
        logging.info('Opening serial port %r', args.port)
    except (serial.SerialException, OSError):
        p = open(args.port, 'r')
        logging.info('Opening file for DIRS data %r', args.port)

    ctx = zmq.Context.instance()
    dstream = ctx.socket(zmq.PUSH)
    dstream.set_hwm(10)
    logging.info('Connecting to server')
    dstream.connect('tcp://{0}:{1}'.format(args.host,
                                           RICS_PORT))

    signal.signal(signal.SIGTERM, signal_handler)

    logging.info('Waiting for data records')
    try:
        while True:
            line = p.readline()
            if not line or line.startswith('X'):
                continue
            logging.debug('RECV: %r', line)
            try:
                for pkt in create_data_packets(line):
                    dstream.send_multipart(pkt, zmq.NOBLOCK)
            except zmq.Again:
                logging.critical('Server down')

    except (KeyboardInterrupt, SystemExit):
        logging.info('Exiting ...')
    except Exception:
        logging.exception('Exiting ... ')
    finally:
        dstream.close(linger=0)
        p.close()


if __name__ == '__main__':
    main()
