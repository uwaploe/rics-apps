#!/usr/bin/env python
#
"""
RICS data manager process.
"""
import sys
import signal
import zmq
import argparse
import logging
import struct
import json
import time
import os
import sqlite3
import serial
from threading import Thread
from collections import defaultdict


RICS_PORT = 10130
PUB_PORT = 10132
CTL_PORT = 10133


def signal_handler(signal, frame):
    sys.exit(0)


def rads_record(t, nd, platform, unit, sep='\t', rtr_order=()):
    """
    Generate timestamp data records for RADS. One record is produced
    for each 'detection'.

    :param t: detection time in seconds since 1/1/1970
    :param nd: map node IDs to detection count.
    :param platform: map node IDs to platform timestamps
    :param unit: map node IDs to unit timestamps.
    :param rtr_order: order of RTR nodes
    """
    # Order of timestamps from the nodes.
    if rtr_order:
        nodes = (129, 130, 131, 132) + tuple(rtr_order)
    else:
        nodes = (129, 130, 131, 132, 1, 2, 3, 4)
    dt = time.strftime('%d/%m/%Y %H:%M:%S', time.gmtime(t))
    # Total number of records to return
    try:
        n = max(nd.values())
    except ValueError:
        n = 1

    rec = [dt]
    for i in range(n):
        for node in nodes:
            ts = platform.get(node)
            if ts and nd[node] > i:
                rec.append(ts[i])
            else:
                rec.append('-1')
        for node in nodes:
            ts = unit.get(node)
            if ts and nd[node] > i:
                rec.append(ts[i])
            else:
                rec.append('-1')
        yield sep.join(rec)
        rec = [dt]


def rads_output(port, store=False, context=None, eol=b'\r\n',
                rtr_order=()):
    """
    Thread to format the timestamp records for RADS and output
    to a serial port.

    Reads time-stamp messages from a ROUTER socket and combines all
    messages from the same second into a single output record for
    the RADS computer.
    """
    ctx = context or zmq.Context.instance()
    dq = ctx.socket(zmq.ROUTER)
    # Connect to the data queue
    dq.connect('inproc://ts')

    # Accumulate detection counts.
    nd = defaultdict(int)
    # Accumulate platform timestamps.
    pdata = defaultdict(list)
    # Accumlate unit timestamps
    udata = defaultdict(list)
    last_t = 0
    trigger = b'X\r\n'
    logging.info('Starting')
    while True:
        sender, body = dq.recv_multipart()
        msg = json.loads(body)
        if last_t == 0:
            last_t = msg['time']
        # A change in the time of the incoming message triggers
        # the creation of a new output record.
        if msg['time'] > last_t:
            count = 0
            for rec in rads_record(last_t, nd, pdata, udata,
                                   rtr_order=rtr_order):
                port.write(rec.encode('us-ascii') + eol)
                if store:
                    dq.send_multipart([sender,
                                       json.dumps({'time': last_t,
                                                   'contents': rec})])
                count += 1
            # Signal RADS to process the input
            if count > 0:
                port.write(trigger)
            logging.debug('Output %d records @%d', count, last_t)
            # Clear the accumulators
            pdata = defaultdict(list)
            udata = defaultdict(list)
            nd = defaultdict(int)
            last_t = msg['time']

        if msg['time'] == last_t:
            # Add the contents of this message to the accumulators.
            nd[msg['rtr']] += len(msg['platform'])
            pdata[msg['rtr']].extend(msg['platform'])
            udata[msg['rtr']].extend(msg['unit'])
        else:
            logging.critical('Late time-stamp: %r', msg)


def db_store(dbdir, fname_tmpl, dbschema,
             puburl=None, rollover=86400, context=None):
    """
    Thread to write the contents of various data messages to
    an SQL database.
    """
    # Map message type to DB table and columns
    tables = {
        'GPS': ['gps', ('rtr', 'time', 'lat', 'lon', 'mode')],
        'ENG': ['eng', ('rtr', 'time', 'vbatt', 'ibatt', 'temp')],
        'DIAG': ['diag', ('rtr', 'time', 'contents')],
        'RADS': ['rads', ('time', 'contents')],
        'GRID': ['grid', ('time', 'lat', 'lon', 'az')],
        'XYPOS': ['xypos', ('rtr', 'time', 'x', 'y')],
        'TRACK': ['track', ('time', 'target', 'lat', 'lon', 'x', 'y')]
    }
    # Create SQL insert statement templates
    stmt = 'INSERT into {0} VALUES ({1})'
    stmts = {}
    for k, v in tables.items():
        tname, cols = v
        qmarks = ['?'] * len(cols)
        stmts[k] = stmt.format(tname, ','.join(qmarks))

    # Create the initial database file.
    logging.info('Databases will be stored under %s', dbdir)
    ts = divmod(time.time(), rollover)
    fname = time.strftime(fname_tmpl, time.gmtime(ts[0]*rollover))
    db_conn = sqlite3.connect(os.path.join(dbdir, fname))
    logging.info('New DB file %s', fname)
    c = db_conn.cursor()
    c.executescript(dbschema)

    # Connect to the publisher
    ctx = context or zmq.Context.instance()
    sub = ctx.socket(zmq.SUB)
    sub.connect(puburl or 'tcp://localhost:{0:d}'.format(PUB_PORT))
    # Subscribe to every message type that we have a table for.
    for mtype in tables.keys():
        sub.setsockopt(zmq.SUBSCRIBE, mtype.encode('us-ascii'))

    logging.info('Starting')
    while True:
        mtype, body = sub.recv_multipart()
        msg = json.loads(body)
        # The contents of the DIAG message will be stored
        # as a JSON blob in the database table.
        if mtype == 'DIAG':
            msg['contents'] = json.dumps(msg['contents'])

        # Is it time to create a new DB file?
        t = divmod(time.time(), rollover)
        if t[0] > ts[0]:
            ts = t
            fname = time.strftime(fname_tmpl, time.gmtime(ts[0]*rollover))
            db_conn.close()
            db_conn = sqlite3.connect(os.path.join(dbdir, fname))
            c = db_conn.cursor()
            c.executescript(dbschema)
            logging.info('New DB file %s', fname)

        try:
            _, tcols = tables[mtype]
            c.execute(stmts[mtype], tuple((msg[k] for k in tcols)))
            db_conn.commit()
        except KeyError:
            logging.info('Unknown message type: %r', mtype)
        except Exception:
            logging.exception('DB ERROR')


def parse_GPS(contents):
    """
    Parse the contents of a GPS data-stream message.
    """
    names = ('lat', 'lon', 'mode')
    d = dict(zip(names, contents.split()))
    d['mode'] = int(d['mode'])
    return d


def parse_ENG(contents):
    """
    Parse the contents of an Engineering data-stream message.
    """
    names = ('vbatt', 'ibatt', 'temp')
    vals = (f.split()[0] for f in contents.split(','))
    return dict(zip(names, vals))


def parse_DIAG(contents):
    """
    Parse the contents of a Diagnostic data-stream message.
    """
    d = {}
    for group in contents.split(';'):
        if group:
            name, val = group.strip().split('=', 1)
            if ',' in val:
                d[name.strip()] = val.strip().split(', ')
            else:
                d[name.strip()] = val.strip()
    return {'contents': d}


def parse_TS(contents):
    """
    Parse the contents of a Timestamp data-stream message.
    """
    p = []
    u = []
    for group in contents.split(';'):
        if group:
            t = group.split()
            p.append(t[0])
            u.append(t[1])
    return dict(platform=p, unit=u)


def parse_GRID(contents):
    """
    Parse the contents of a Grid data-stream message.
    """
    return json.loads(contents)


def parse_XYPOS(contents):
    """
    Parse the contents of an Xypos data-stream message.
    """
    return json.loads(contents)


def parse_TRACK(contents):
    """
    Parse the contents of a Track data-stream message.
    """
    return json.loads(contents)


def main():
    parser = argparse.ArgumentParser(description=__doc__,
                                     fromfile_prefix_chars='@')
    parser.add_argument('port',
                        help='Serial port device for RADS output')
    parser.add_argument('schema',
                        help='Database schema file')
    parser.add_argument('--baud', '-b',
                        type=int,
                        default=115200,
                        help='Serial port baud rate (%(default)s)')
    parser.add_argument('--dbdir', '-d',
                        default='.',
                        help='Database directory (%(default)s)')
    parser.add_argument('--rollover', '-r',
                        type=int,
                        metavar='SECS',
                        default=86400,
                        help='Database rollover time in seconds (%(default)s)')
    parser.add_argument('--template', '-t',
                        default='ricsdata_%Y%m%d.db',
                        help='Database filename template (%(default)s)')
    parser.add_argument('--store-rads', '-s',
                        action='store_true',
                        help='store RADS records in the database')
    parser.add_argument('--data-port',
                        type=int,
                        default=RICS_PORT,
                        help='TCP port for data-stream (%(default)s)')
    parser.add_argument('--pub-port',
                        type=int,
                        default=PUB_PORT,
                        help='TCP port for publisher (%(default)s)')
    parser.add_argument('--cmd-url',
                        default='tcp://localhost:{0:d}'.format(CTL_PORT),
                        help='url for Controller (%(default)s)')
    parser.add_argument('--verbose', '-v',
                        action='store_true',
                        help='show debugging output')
    parser.add_argument('--heartbeat',
                        type=int,
                        default=5,
                        help='Heart-beat interval for RTR nodes (%(default)s)')
    parser.add_argument('--order',
                        metavar='LIST',
                        default='1,2,3,4',
                        help='Order of RTR nodes in RADS output (%(default)s)')
    args = parser.parse_args()

    if args.verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(level=level,
                        format='%(levelname)-8s %(threadName)-10s %(message)s')

    try:
        schema = open(args.schema, 'r').read()
    except Exception:
        logging.exception('Cannot read DB schema')
        sys.exit(1)

    try:
        p = serial.Serial(args.port, baudrate=args.baud,
                          timeout=0)
        logging.info('Opening serial port %s', args.port)
    except (serial.SerialException, OSError):
        p = open(args.port, 'w')
        logging.info('Opening file for RADS data %s', args.port)

    ctx = zmq.Context.instance()
    ctx.linger = 0
    dstream = ctx.socket(zmq.PULL)
    dstream.bind('tcp://*:{0:d}'.format(args.data_port))
    pub = ctx.socket(zmq.PUB)
    pub.set_hwm(32)
    pub.bind('tcp://*:{0:d}'.format(args.pub_port))
    dq = ctx.socket(zmq.DEALER)
    dq.bind('inproc://ts')

    logging.info('Connecting to Controller')
    ctl = ctx.socket(zmq.DEALER)
    ctl.set_hwm(1)
    ctl.connect(args.cmd_url)

    # Start thread to format timestamp records for RADS
    thr = Thread(target=rads_output,
                 name='RADS',
                 args=(p,),
                 kwargs={'context': ctx,
                         'store': args.store_rads,
                         'rtr_order': tuple(int(s)
                                            for s in args.order.split(','))})
    thr.daemon = True
    thr.start()

    # Start database storage thread
    thr = Thread(target=db_store,
                 name='DB_store',
                 args=(args.dbdir, args.template, schema),
                 kwargs={'context': ctx, 'rollover': args.rollover})
    thr.daemon = True
    thr.start()

    poller = zmq.Poller()
    poller.register(dstream, zmq.POLLIN)
    poller.register(dq, zmq.POLLIN)
    poller.register(ctl, zmq.POLLIN)

    unpacker = struct.Struct('>cBll')
    hdr_vars = ('rtr', 'time', 'tusecs')

    tags = {
        b'T': 'TS',
        b'P': 'GPS',
        b'E': 'ENG',
        b'D': 'DIAG',
        b'G': 'GRID',
        b'X': 'XYPOS',
        b'R': 'TRACK'
    }

    signal.signal(signal.SIGTERM, signal_handler)

    heartbeats = {}
    G = globals()
    try:
        logging.info('Waiting for messages')
        while True:
            socks = dict(poller.poll(args.heartbeat*1000))
            if socks.get(dstream) == zmq.POLLIN:
                hdr, body = dstream.recv_multipart()
                logging.debug('>PULL: [%r] %r', hdr, body)
                vals = unpacker.unpack(hdr)
                # The first value in the header is the message type
                # code, we don't include that in the outgoing message.
                msg = dict(zip(hdr_vars, vals[1:]))
                tag = tags.get(vals[0], '')
                if tag:
                    f = G.get('parse_'+tag)
                    if f is not None:
                        msg.update(f(body))
                        logging.debug('<PUB: %s %r', tag, msg)
                        body = json.dumps(msg)
                        pub.send_multipart([tag.encode('us-ascii'), body])
                        if tag == 'TS':
                            dq.send(body)
                            if msg['rtr'] < 128:
                                heartbeats[msg['rtr']] = time.time()

            if socks.get(dq) == zmq.POLLIN:
                # This message is already JSON encoded
                body = dq.recv()
                pub.send_multipart([b'RADS', body])

            if socks.get(ctl) == zmq.POLLIN:
                resp = ctl.recv_multipart()
                logging.info('>DEALER: %r', resp)

            # Check that RTRs are still sending data (heartbeat check)
            t = time.time()
            lost = []
            for k, v in heartbeats.items():
                if (t - v) > args.heartbeat:
                    lost.append(str(k).encode('us-ascii'))
                    del heartbeats[k]

            if lost:
                logging.info('RTRs offline: %r', lost)
                try:
                    ctl.send_multipart([json.dumps(t),
                                        b'',
                                        b'FORGET',
                                        b' '.join(lost)],
                                       flags=zmq.NOBLOCK)
                except zmq.ZMQError:
                    logging.info('Controller offline')

    except (KeyboardInterrupt, SystemExit):
        logging.info('Exiting ...')
    except Exception:
        logging.exception('Exiting ... ')
    finally:
        pub.close(linger=0)
        dstream.close(linger=0)
        ctl.close(linger=0)
        p.close()


if __name__ == '__main__':
    main()
