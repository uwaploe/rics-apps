#!/usr/bin/env python
#
import unittest
import time
import struct
from collections import defaultdict
from dirsmon import create_data_packets, packer
from datamgr import parse_TS, parse_DIAG, parse_ENG, rads_record


DIRSREC = '11/03/2013 13:27:53 -1 -1 -1 -1 -1 -1 -1 -1 913 130 910 347 -1 -1 -1 -1'

RESULTS = [
    ('2013-03-11T13:27:53', '-1 913'),
    ('2013-03-11T13:27:53', '-1 130'),
    ('2013-03-11T13:27:53', '-1 910'),
    ('2013-03-11T13:27:53', '-1 347')
]


DIAG = """varAOutput = 4.143e-08, 5.414e-08, 3.560e-08
varACurrent = 3.337e-04
threshold_A = 1.000e+02
firstFrameIndA = -1, -1
peakAOut = -0, -0
varBOutput = 4.149e-07, 3.321e-07, 1.447e-07
varBCurrent = 0.000e+00
threshold_B = 2.802e-03
firstFrameIndB = 5222, -1
peakBOut = 5221, 5231
timeOffset = 457.799988"""

ENG = "1395009046.999791,24.25 volts,0.6954 amps,-0.5 degC"


class TestDataPacket(unittest.TestCase):

    def setUp(self):
        self.tfmt = '%Y-%m-%dT%H:%M:%S'
        self.diag = ';'.join(DIAG.split('\n'))
        _, self.eng = ENG.split(',', 1)

    def test_pack(self):
        i = 0
        for hdr, data in create_data_packets(DIRSREC):
            mtype, node_id, secs, usecs = struct.unpack('>cBll', hdr)
            ts = time.strftime(self.tfmt, time.gmtime(secs))
            t, vals = RESULTS[i]
            self.assertEqual(mtype, b'T', 'Bad message type code')
            self.assertEqual(ts, t, 'Bad timestamp: {0}'.format(ts))
            self.assertEqual(vals, data, 'Bad data: {0!r}'.format(data))
            self.assertEqual(node_id, 0x81+i, 'Bad node ID')
            i += 1
        self.assertEqual(i, 4, 'Bad packet count')

    def test_error(self):
        pkts = list(create_data_packets(DIRSREC[2:]))
        self.assertEqual(len(pkts), 0, 'Error not caught')

    def test_extract_ts(self):
        p = defaultdict(list)
        u = defaultdict(list)
        nd = defaultdict(int)
        t = 0
        for hdr, data in create_data_packets(DIRSREC):
            mtype, node_id, secs, usecs = struct.unpack('>cBll', hdr)
            d = parse_TS(data)
            p[node_id].extend(d['platform'])
            u[node_id].extend(d['unit'])
            nd[node_id] += 1
            t = secs
        recs = list(rads_record(t, nd, p, u, sep=' '))
        self.assertEqual(len(recs), 1, 'Bad record count')
        self.assertItemsEqual(recs[0], DIRSREC)

    def test_extract_diag(self):
        d = parse_DIAG(self.diag)
        self.assertEqual(d['contents']['firstFrameIndA'], '-1, -1',
                         'Bad diag data: {0!r}'.format(d))

    def test_extract_eng(self):
        d = parse_ENG(self.eng)
        self.assertEqual('0.6954', d['ibatt'], 'Bad eng data: {0!r}'.format(d))


class TestRADSRecord(unittest.TestCase):
    def setUp(self):
        self.p = defaultdict(list)
        self.u = defaultdict(list)
        self.nd = defaultdict(int)
        self.t = 0
        self.empty = ['01/01/1970 00:00:00'] + ['-1'] * 16

    def test_empty(self):
        recs = list(rads_record(self.t, self.nd, self.p, self.u))
        self.assertEqual(len(recs), 1, 'Bad record count')
        empty = '\t'.join(self.empty)
        self.assertItemsEqual(recs[0], empty)

    def test_sparse(self):
        self.p[0x81].extend(['42', '42'])
        self.u[0x81].extend(['42', '42'])
        self.nd[0x81] = 2
        recs = list(rads_record(self.t, self.nd, self.p, self.u))
        self.assertEqual(len(recs), 2, 'Bad record count')
        self.assertItemsEqual(recs[0], recs[1])


if __name__ == '__main__':
    for klass in (TestDataPacket, TestRADSRecord):
        suite = unittest.TestLoader().loadTestsFromTestCase(klass)
        unittest.TextTestRunner(verbosity=2).run(suite)
